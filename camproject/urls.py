"""camproject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from campapp import views
from django.conf.urls.static import static
from django.conf import settings
urlpatterns = [
    path('admin/', admin.site.urls),
    path('dashcom/',views.dash,name='dashcomp'),
    path('dashstu/',views.dashstu,name='dashstu'),
    path('alljobs/',views.jobs,name='jobs'),
    path('job/',views.job,name='job'),
    path('postjob/',views.postjob,name='postjob'),
    path('chngpass/',views.chngpass,name='pass'),
    path('chngpass1/',views.chngpass1,name='passw'),
    path('edit/',views.edit,name='edit'),
    path('latestvac/',views.latestvac,name='vacancy'),
    path('latestcomp/',views.latestcomp,name='companies'),
    path('applicants/',views.applicants,name='applicants'),
    path('chngpass1/',views.passw,name='passw'),
    path('edit1/',views.editp,name='editp'),
    path('editpr/',views.editpr,name='editpr'),
    path('editstu/',views.editstu,name='editstu'),
    path('appliedvacancy/',views.applyvac,name='vac'),
    path('sendmail/',views.sendmail,name='sndm'),
    
    path('',views.campus,name='campus'),
    path('about/',views.about,name='about'),

    path('contact/',views.contactView,name='contactpage'),
    
    path('home/',views.home,name='home'),
    path('minimal/',views.compdes,name='compdes'),
 
    #path('pydev/',views.pydev,name='pydev'),
    path('seeresult/',views.seeresult,name='seeresult'),
    path('quizresult/',views.quizresult,name='quizresult'),

    path('jobdes/',views.jobdes,name='jobdes'),
    #path('compdes/',views.compdes,name='compdes'),
    path('anchor/',views.anchor,name='anchor'),
    path('intelli/',views.intelli,name='intelli'),
    #path('graphics/',views.graphics,name='graphics'),
    #path('dbase/',views.dbase,name='dbase'),
    #path('civil/',views.civil,name='civil'),
    path('hom/',views.hom,name='hom'),

    path('login/',views.uslogin,name='login'),
    path('uslogout/',views.uslogout,name='uslogout'),
    path('signup/',views.signup,name='signup'),
    path('postjob/',views.postjob,name='postjob'),
    
    path('civil/',views.civil,name='civi'),
    path('graph/',views.grap,name='graph'),
    path('python/',views.pyth,name='pyt'),
    path('android/',views.andr,name='anr'),
    path('netw/',views.net,name='netr'),
    path('dbase/',views.dbas,name='dbase'),
    path('deletejob/',views.delete_post,name='delete'),
    path('editpost/',views.edit_post,name='editpost'),
    path('check_user',views.check_user,name="check_user"),
    # path('forgot/',views.forgot,name='fp'),
    # path('finduser/',views.finduser,name='finduser'),
    path('fg/',views.forgot,name='fg'),
    path('finduser/',views.finduser,name='finduser'),
]+static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)
